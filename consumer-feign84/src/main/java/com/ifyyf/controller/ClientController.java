package com.ifyyf.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ifyyf.consumer.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.Objects;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-05 下午 10:17
 */
@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/consumer/payment/{string}")
    @SentinelResource(
            value = "hello",
            blockHandler = "blockHandler",
            fallback="fallback")
    public String hello(@PathVariable String string){
        if(Objects.equals("exception",string)){
            //模拟抛出异常
            throw new NullPointerException();
        }
        return clientService.hello(string);
    }
    public String blockHandler(String string,BlockException blockException){
        return "服务挂了没关系，还有我来处理-> blockHandler";
    }
    public String fallback(String string,Throwable throwable){
        return "出现异常了没关系，还有我来处理-> fallback";
    }
}
