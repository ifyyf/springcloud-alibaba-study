package com.ifyyf.config;

import com.ifyyf.consumer.ClientService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Author if
 * @Description: 服务降级配置类，需要返回feign被降级的接口实例
 * 降级的实现方法写在接口类的实现方法上
 * @Date 2021-12-05 下午 10:15
 */
@Component
public class ClientServiceFallbackFactory implements FallbackFactory {
    @Override
    public ClientService create(Throwable throwable) {
        return new ClientService() {
            @Override
            public String hello(String string) {
                return "服务暂时不可用，Feign引用了服务降级方法";
            }
        };
    }
}
