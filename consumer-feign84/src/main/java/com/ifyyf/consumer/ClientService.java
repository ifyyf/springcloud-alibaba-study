package com.ifyyf.consumer;

import com.ifyyf.config.ClientServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-05 下午 10:11
 */
@FeignClient(value = "nacos-payment-provider",fallbackFactory = ClientServiceFallbackFactory.class)
@Service
public interface ClientService {
    @GetMapping("/payment/{string}")
    String hello(@PathVariable("string") String string);
}
