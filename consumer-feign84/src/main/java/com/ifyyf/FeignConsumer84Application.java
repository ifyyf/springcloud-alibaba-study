package com.ifyyf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-05 下午 10:13
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.ifyyf"})
public class FeignConsumer84Application {
    public static void main(String[] args) {
        SpringApplication.run(FeignConsumer84Application.class,args);
    }
}
