package com.ifyyf.storage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ifyyf.storage.domain.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;


/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:21
*/
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
    void decrease(@Param("userId") Long userId,@Param("money") BigDecimal money);
}