package com.ifyyf.storage.service;

import com.ifyyf.storage.domain.Account;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:21
*/
public interface AccountService extends IService<Account>{

    void decrease(Long userId, BigDecimal money);
}
