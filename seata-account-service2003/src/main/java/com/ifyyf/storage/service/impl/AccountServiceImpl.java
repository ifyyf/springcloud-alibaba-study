package com.ifyyf.storage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ifyyf.storage.domain.Account;
import com.ifyyf.storage.dao.AccountMapper;
import com.ifyyf.storage.service.AccountService;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:21
*/
@Service
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService{
    @Resource
    private AccountMapper accountDao;

    @Override
    public void decrease(Long userId, BigDecimal money) {
        log.info("账户扣除余额开始---");

        //模拟超时
//        try{
//            Thread.sleep(3000);
//        }catch(Exception e){
//
//        }
        accountDao.decrease(userId, money);
        log.info("账户扣除余额结束---");
    }
}
