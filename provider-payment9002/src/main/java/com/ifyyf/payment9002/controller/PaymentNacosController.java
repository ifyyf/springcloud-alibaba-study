package com.ifyyf.payment9002.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-04 下午 09:31
 */
@RestController
public class PaymentNacosController {

    @GetMapping("/payment/{string}")
    public String hello(@PathVariable String string){
        return "Hello 9002 ->"+string;
    }
}
