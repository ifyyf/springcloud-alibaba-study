package com.ifyyf.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-04 下午 10:08
 */
@RestController
//支持nacos配置的动态刷新功能
@RefreshScope
public class ConfigClientController {

    //这里读取配置中心的配置内容
    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }
}
