package com.ifyyf.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-04 下午 10:07
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosClient3377Application {
    public static void main(String[] args) {
        SpringApplication.run(NacosClient3377Application.class,args);
    }
}
