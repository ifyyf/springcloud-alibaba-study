package com.ifyyf.order83;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-04 下午 09:16
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosOrder83Application {
    public static void main(String[] args) {
        SpringApplication.run(NacosOrder83Application.class, args);
    }
}
