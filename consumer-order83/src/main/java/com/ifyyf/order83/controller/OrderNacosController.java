package com.ifyyf.order83.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import javax.annotation.Resource;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-04 下午 09:24
 */
@RestController
@Slf4j
public class OrderNacosController {

    @Resource
    private RestTemplate restTemplate;

    /**consumer消费者将要去访问的服务名称(前提是已经注册进了nacos)*/
    private static final String SERVICE_URL ="http://nacos-payment-provider";

    @GetMapping("/consumer/payment/{string}")
    public String paymentInfo(@PathVariable String string){
        return restTemplate.getForObject(SERVICE_URL +"/payment/"+string,String.class);
    }
}
