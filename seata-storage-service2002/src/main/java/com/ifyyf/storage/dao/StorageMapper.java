package com.ifyyf.storage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ifyyf.storage.domain.Storage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:24
*/
@Mapper
public interface StorageMapper extends BaseMapper<Storage> {
    void decrease(@Param("productId") Long productId,@Param("count") Integer count);
}