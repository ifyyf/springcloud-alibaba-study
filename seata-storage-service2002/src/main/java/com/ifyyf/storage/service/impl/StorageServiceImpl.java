package com.ifyyf.storage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ifyyf.storage.domain.Storage;
import com.ifyyf.storage.dao.StorageMapper;
import com.ifyyf.storage.service.StorageService;

/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:24
*/
@Service
@Slf4j
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements StorageService{

    @Resource
    private StorageMapper storageDao;

    @Override
    public void decrease(Long productId, Integer count) {
        log.info("库存扣减开始----");
        storageDao.decrease(productId,count);
        log.info("库存扣减结束----");
    }
}
