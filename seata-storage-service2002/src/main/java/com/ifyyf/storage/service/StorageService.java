package com.ifyyf.storage.service;

import com.ifyyf.storage.domain.Storage;
import com.baomidou.mybatisplus.extension.service.IService;
    
/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 09:24
*/
public interface StorageService extends IService<Storage>{
    void decrease(Long productId,Integer count);

}
