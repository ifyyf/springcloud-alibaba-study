package com.ifyyf.order.controller;

import com.ifyyf.order.domain.CommonResult;
import com.ifyyf.order.domain.Order;
import com.ifyyf.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-07 下午 08:27
 */
@RestController
public class OrderController {

    @Autowired()
    private OrderService orderService;

    @GetMapping("/order/create")
    public CommonResult create(Order order) {
        orderService.create(order);
        return new CommonResult(200,"订单创建成功！");
    }

}
