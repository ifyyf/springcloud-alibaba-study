package com.ifyyf.order.service.impl;

import com.ifyyf.order.service.AccountService;
import com.ifyyf.order.service.OrderService;
import com.ifyyf.order.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ifyyf.order.dao.OrderMapper;
import com.ifyyf.order.domain.Order;
import javax.annotation.Resource;

/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 08:21
*/
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private StorageService storageService;
    @Resource
    private AccountService accountService;

    @GlobalTransactional(name = "fsp-create-order",rollbackFor = Exception.class)
    @Override
    public void create(Order order) {
        log.info("-------->开始创建新订单");
        orderMapper.create(order);

        log.info("--------订单微服务开始调用库存,做扣减");
        storageService.decrease(order.getProductId(),order.getCount());
        log.info("-------订单微服务开始调用库存，做扣减end");

        log.info("-------订单微服务开始调用账户，做扣减");
        accountService.decrease(order.getUserId(),order.getMoney());
        log.info("-------订单微服务开始调用账户，做扣减end");

        log.info("-------修改订单状态");
        orderMapper.update(order.getUserId(),0);
        log.info("-------修改订单状态结束");

        log.info("--------下订单结束");
    }
}
