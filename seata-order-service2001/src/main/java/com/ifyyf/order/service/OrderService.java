package com.ifyyf.order.service;

import com.ifyyf.order.domain.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 08:21
*/

public interface OrderService extends IService<Order>{
    void create(Order order);
}
