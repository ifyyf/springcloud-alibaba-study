package com.ifyyf.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ifyyf.order.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
* @Author  if
* @Description: What is it
* @Date  2021-12-07 下午 08:21
*/
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    //创建订单
    void create(Order order);

    //修改订单状态
    void update(@Param("userId") Long userId,@Param("status") Integer status);
}