package com.ifyyf.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-07 下午 08:12
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class OrderService2001Application {
    public static void main(String[] args) {
        SpringApplication.run(OrderService2001Application.class,args);
    }
}
