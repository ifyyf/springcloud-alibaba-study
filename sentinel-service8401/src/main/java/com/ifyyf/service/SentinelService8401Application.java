package com.ifyyf.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-05 下午 02:05
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelService8401Application {
    public static void main(String[] args) {
        SpringApplication.run(SentinelService8401Application.class,args);
    }
}
