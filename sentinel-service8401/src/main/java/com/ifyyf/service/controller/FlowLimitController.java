package com.ifyyf.service.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-12-05 下午 02:06
 */
@RestController
public class FlowLimitController {

    @GetMapping("/a")
    public String testA(){
        return "testA！";
    }

    @GetMapping("/b")
    @SentinelResource(
            value = "my",
            //sentinel的降级限流
            blockHandler = "blockHandler",
            //业务异常
            fallback="fallback",
            //忽略的异常
            exceptionsToIgnore=NullPointerException.class)
    public String testB(){
        return "testB！";
    }

    public String blockHandler(BlockException blockException){
        return "服务挂了没关系，还有我呢来处理-> blockHandler";
    }
    public String fallback(Throwable throwable){
        return "服务挂了没关系，还有我呢来处理-> fallback";
    }
}
